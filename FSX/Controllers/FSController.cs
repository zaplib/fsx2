﻿using FSX.Controllers.Extensions;
using FSX.Lib;
using FSX.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using ZapLib;
using ZapLib.Model;
using ZapLib.Utility;
using System.Linq;
using System.Diagnostics;
using System.Web;

namespace FSX.Controllers
{

    [RoutePrefix("api")]
    public class FSController : ApiController
    {
        //const int maxImgSize = 1024;
        const int maxThumbSize = 240;

        [HttpPost]
        [Route("upload")]
        public HttpResponseMessage upload()
        {
            ExtApiHelper api = new ExtApiHelper(this);
            I3SFileSystem fs = new I3SFileSystem();
            ModelUpload body;

            try
            {
                body = api.GetJsonBody<ModelUpload>();
            }
            catch (Exception) { body = null; }


            string tmpDir = Config.Get("Storage");
            string tmpPath;
            string fileName;

            if (body == null)
            {
                List<ModelFile> files = UploadFile();
                if (files == null) return api.GetResponse(new { error = "can not upload file" }, HttpStatusCode.Forbidden);
                tmpPath = tmpDir + "\\" + files[0].name;
                fileName = files[0].des;
            }
            else
            {
                string tmpName = Guid.NewGuid().ToString();
                fileName = body.name;
                tmpPath = tmpDir + "\\" + tmpName;
                using (var fsm = new FileStream(tmpPath, FileMode.Create, FileAccess.Write))
                {
                    fsm.Write(body.file, 0, body.file.Length);
                }
            }

            var res = fs.register(fileName);
            if (res == null) return api.GetResponse(new { error = "can not register file in I3S FileSystem" }, HttpStatusCode.Forbidden);

            File.Move(tmpPath, res.dist);

            InfoExtension iext = new InfoExtension(res.uuid);
            iext.SetController(this);


            if (res.mimeType.Contains("image"))
            {
                ImageTool it = new ImageTool(res.dist);
                int ImageCompression = Cast.To(Config.Get("ImageCompression"), 1024);
                it.scale(ImageCompression);
            }

            if (res.mimeType.Contains("video"))
            {
                var thumbPath = res.dist + ".thumb";
                VideoTool vt = new VideoTool(res.dist);
                vt.createThumbnail(thumbPath);
                ImageTool it = new ImageTool(thumbPath);
                it.scale(maxThumbSize);
            }

            return api.GetResponse(iext.GetFileInfo());
        }

        [HttpGet]
        [Route("download")]
        [Route("download/{uuid}")]
        public HttpResponseMessage download(string uuid = null, bool info = false, string type = null)
        {
            ExtApiHelper api = new ExtApiHelper(this);
            if (info && !string.IsNullOrWhiteSpace(uuid)) return InfoExtension(uuid);
            if (!string.IsNullOrWhiteSpace(uuid)) return DefaultExtension(uuid);
            if (string.IsNullOrWhiteSpace(type)) return api.GetResponse(new { error = "wrong params" }, HttpStatusCode.Forbidden);

            Extension extension = ExtensionFactory(type);
            if (extension == null) return api.GetResponse(new { error = "no extension" }, HttpStatusCode.Forbidden);

            extension.SetController(this);
            return extension.GetResponse();
        }


        [HttpGet]
        [Route("thumbnail")]
        [Route("thumbnail/{uuid}")]
        public HttpResponseMessage thumbnail(string uuid, bool info = false)
        {
            I3SFileSystem fs = new I3SFileSystem();
            var file = fs.getRegisterFile(uuid);
            bool exist = File.Exists(file.dist + ".thumb");
            string thumbPath = exist ? (file.dist + ".thumb") : (Config.Get("Storage") + "\\play.png");
            string mimeType = exist ? "image/jpg" : "image/png";
            return responseFile(thumbPath, mimeType, file.filename);
        }

        [NonAction]
        private HttpResponseMessage responseFile(string filePath, string type, string filename)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            if (filePath != null && File.Exists(filePath))
            {
                response.Content = new StreamContent(File.OpenRead(filePath));
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(type);
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = filename;
            }
            return response;
        }

        [NonAction]
        private HttpResponseMessage DefaultExtension(string uuid)
        {
            DefaultExtension ext = new DefaultExtension(uuid);
            ext.SetController(this);
            return ext.GetResponse();
        }

        [NonAction]
        private HttpResponseMessage InfoExtension(string uuid)
        {
            InfoExtension ext = new InfoExtension(uuid);
            ext.SetController(this);
            return ext.GetResponse();
        }

        [NonAction]
        private Extension ExtensionFactory(string typeName)
        {
            try
            {
                foreach (Type tp in Mirror2.GetClasses<Extension>())
                {
                    var attr = Mirror2.GetCustomAttributes<ExtensionAttribute>(tp).FirstOrDefault();
                    if (attr == null) continue;
                    if (typeName == attr.TypeName)
                    {
                        ClassMirror cm = new ClassMirror(tp);
                        return (Extension)cm.Instance;
                    }
                }
            }
            catch (Exception e)
            {
                MyLog log = new MyLog();
                log.SilentMode = Config.Get("SilentMode");
                log.Write(e.ToString());
                Trace.WriteLine(e.ToString());
            }
            return null;
        }


        [NonAction]
        private List<ModelFile> UploadFile()
        {
            string dest = Config.Get("Storage");
            MyLog log = new MyLog();
            log.SilentMode = Config.Get("SilentMode");
            HttpFileCollection files = HttpContext.Current.Request.Files;
            if (files.Count < 1) return null;
            var filelist = new List<ModelFile>();
            foreach (string key in files)
            {
                HttpPostedFile file = files[key];
                string fileName = Path.GetFileName(file.FileName);
                string path = Path.Combine(HttpContext.Current.Server.MapPath("~"), fileName);
                string newName = string.Format("{0}_{1}", Guid.NewGuid().ToString(), fileName);
                string destPath = string.Format("{0}/{1}", dest, newName);
                file.SaveAs(path);
                File.Move(path, destPath);
                FileInfo info = new FileInfo(destPath);
                filelist.Add(new ModelFile()
                {
                    name = newName,
                    oid = 0,
                    des = fileName,
                    size = info.Length,
                    type = MimeMapping.GetMimeMapping(fileName)
                });
            }
            return filelist.Count < 1 ? null : filelist;
        }

    }

}
