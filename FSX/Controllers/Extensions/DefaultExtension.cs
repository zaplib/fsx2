﻿using FSX.Lib;
using FSX.Lib.HttpStream;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using ZapLib;
using ZapLib.Utility;

namespace FSX.Controllers.Extensions
{
    public class DefaultExtension : Extension
    {
        private string uuid;

        public DefaultExtension(string uuid)
        {
            this.uuid = uuid;
        }

        public override HttpResponseMessage GetResponse()
        {
            ExtApiHelper api = new ExtApiHelper(Controller);
            I3SFileSystem fs = new I3SFileSystem();

            var file = fs.getRegisterFile(uuid);
            if (file == null) return api.GetResponse(new { error = "invalid uuid" }, HttpStatusCode.Forbidden);
            if (!File.Exists(file.dist)) return  api.GetResponse(new { error = "can not find file" }, HttpStatusCode.NotFound);

            // 部分內容
            (long StartPoint, long? EndPoint, bool IsPartial) = GetHeaderRange();

            Trace.WriteLine($"[API Request] Filename:{file.filename}  Range:{StartPoint}-{EndPoint}");

            // 瀏覽器預先偵測資源是否存在，Accept 將會以 */html 來試探，此時不需要做無意義串流讀取
            // 僅需回應瀏覽器資源是否存在

            //if (Request.Headers.Accept.ToString().Contains("html")) return new ProbeResponse(file.dist).Response;

            // 串流專用回應
            StreamResponse resp = new StreamResponse(file.dist, StartPoint, EndPoint);

            // 從 1MB 緩衝大小改 245,760 byte 2048*120
            string buffer_length_config = Config.Get("BufferLength");
            long buffer_length = Cast.To<long>(buffer_length_config);
            resp.BufferLength = buffer_length_config==null? 245760 : buffer_length;
            // 是否為部分請求
            resp.IsPartial = IsPartial;
            // 原始檔名
            resp.FileName = file.filename;
            // 原始 Mime Type
            resp.MimeType = file.mimeType;
            // 組成串流回應
            resp.StartProcessing();
            // 最終輸出
            return resp.Response;
        }


        /// <summary>
        /// 取得 Header 中的 Range
        /// </summary>
        /// <returns></returns>
        private (long StartPoint, long? EndPoint, bool IsPartial) GetHeaderRange()
        {
            RangeHeaderValue rangeHeader = Controller.Request.Headers.Range;
            // 只有 Range 不是 NULL 且單位為 bytes 且只有一組時，才是 partial content
            if (rangeHeader != null && rangeHeader.Unit == "bytes" && rangeHeader.Ranges.Count == 1)
            {
                RangeItemHeaderValue range = rangeHeader.Ranges.OfType<RangeItemHeaderValue>().FirstOrDefault();
                return (range.From ?? 0, range.To, true);
            }
            // 其餘設定均視為下載全部檔案
            else
                return (0, null, false);
        }
    }
}