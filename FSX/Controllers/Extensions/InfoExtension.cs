﻿using FSX.Lib;
using FSX.Models;
using System.IO;
using System.Net;
using System.Net.Http;
using ZapLib;
using ZapLib.Utility;

namespace FSX.Controllers.Extensions
{
    public class InfoExtension : Extension
    {
        private ExtApiHelper api;
        public ModelRegisterFile I3SFileInfo { get; private set; }

        public InfoExtension(string uuid)
        {
            api = Controller == null ? null : new ExtApiHelper(Controller);
            I3SFileSystem fs = new I3SFileSystem();
            I3SFileInfo = fs.getRegisterFile(uuid);
        }

        public IModelFileInfo GetFileInfo()
        {
            if (api == null) api = new ExtApiHelper(Controller);
            if ((I3SFileInfo == null && !File.Exists(I3SFileInfo.dist))) return null;

            var host = Config.Get("Host") ?? api.GetMyHost();
            var url = host + "/api/download?uuid=" + I3SFileInfo.uuid;

            ModelFileInfo info = new ModelFileInfo()
            {
                mimeType = I3SFileInfo.mimeType,
                uuid = I3SFileInfo.uuid,
                filename = I3SFileInfo.filename,
                length = new FileInfo(I3SFileInfo.dist).Length,
                url = url
            };

            if (I3SFileInfo.mimeType.Contains("image"))
            {
                ImageTool tool = new ImageTool(I3SFileInfo.dist);
                var size = tool.getSize();
                ModelImgInfo imginfo = new ModelImgInfo();
                Mirror.Assign(ref imginfo, info, new
                {
                    width = size.Item1,
                    height = size.Item2
                });
                return imginfo;
            }

            if (I3SFileInfo.mimeType.Contains("video"))
            {
                var thumbnail = host + "/api/thumbnail?uuid=" + I3SFileInfo.uuid;
                ModelVideoInfo videoinfo = new ModelVideoInfo();
                Mirror.Assign(ref videoinfo, info, new
                {
                    thumbnail
                });
                return videoinfo;
            }

            if (I3SFileInfo.mimeType.Contains("audio"))
            {
                AudioTool at = new AudioTool(I3SFileInfo.dist);
                double duration = at.getDuration(I3SFileInfo.mimeType);
                ModelAudioInfo audioinfo = new ModelAudioInfo();
                Mirror.Assign(ref audioinfo, info, new
                {
                    duration
                });
                return audioinfo;
            }
            return info;
        }


        public override HttpResponseMessage GetResponse()
        {
            if (api == null) api = new ExtApiHelper(Controller);
            if (I3SFileInfo == null) return api.GetResponse(new { error = "invalid uuid" }, HttpStatusCode.Forbidden);
            if (!File.Exists(I3SFileInfo.dist)) return api.GetResponse(new { error = "can not find file" }, HttpStatusCode.NotFound);
            return api.GetResponse(GetFileInfo());
        }
    }
}