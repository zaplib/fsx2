﻿using FSX.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ZapLib;

namespace FSX.Controllers
{
    [RoutePrefix("api/test")]
    public class TestController : ApiController
    {
        [HttpGet]
        [Route("info")]
        public HttpResponseMessage info()
        {
            ServerPath p1 = new ServerPath(@"App_Data\play.png");
            ServerPath p2 = new ServerPath(@"SQLite");
            ExtApiHelper api = new ExtApiHelper(this);
            return api.GetResponse(new
            {
                png_path = p1.FilePath,
                png_path_exists = p1.Exist,
                png_isfolder = p1.IsDirectory,
                folder_path = p2.FilePath,
                folder_path_exists = p2.Exist,
                folder_isfolder = p2.IsDirectory,
            });
        }
    }
}
