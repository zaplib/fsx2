﻿using System.Web.Mvc;

namespace FSX.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "FSX 2";

            return View();
        }
    }
}
