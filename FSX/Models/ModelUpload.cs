﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FSX.Models
{
    public class ModelUpload
    {
        public string name { get; set; }
        public byte[] file { get; set; }
    }
}