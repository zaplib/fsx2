﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FSX.Models
{
    public interface IModelFileInfo
    {
        string mimeType { get; set; }
        string uuid { get; set; }
        string filename { get; set; }
        long length { get; set; }
        string url { get; set; }
    }

    public class ModelFileInfo : IModelFileInfo
    {
        public string mimeType { get; set; }
        public string uuid { get; set; }
        public string filename { get; set; }
        public long length { get; set; }
        public string url { get; set; }
    }

    public class ModelVideoInfo : ModelFileInfo, IModelFileInfo
    {
        public string thumbnail { get; set; }
    }

    public class ModelAudioInfo : ModelFileInfo, IModelFileInfo
    {
        public double duration { get; set; }
    }

    public class ModelImgInfo : ModelFileInfo, IModelFileInfo
    {
        public int width { get; set; }
        public int height { get; set; }
    }
}