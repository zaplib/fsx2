﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FSX.Models
{
    public class ModelRegisterFile
    {
        [JsonIgnore]
        public long oid { get; set; }
        public string uuid { get; set; }
        [JsonIgnore]
        public string dist { get; set; }
        public string mimeType { get; set; }
        public string filename { get; set; }
    }
}