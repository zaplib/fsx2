﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FSX.Models
{
    public class ModelRegisterImage:ModelRegisterFile
    {
        public int width { get; set; }
        public int height { get; set; }
    }
}