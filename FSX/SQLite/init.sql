CREATE TABLE Object(
   oid INTEGER PRIMARY KEY AUTOINCREMENT,
   uuid CHAR(36) not null,
   cname CHAR(255) not null
);
CREATE UNIQUE INDEX uq_uuid on Object (uuid);