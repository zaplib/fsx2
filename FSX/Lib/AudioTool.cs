﻿namespace FSX.Lib
{
    public class AudioTool
    {
        private string audioPath;
        public AudioTool(string audioPath) {
            this.audioPath = audioPath;
        }
        public double getDuration(string mimeType) {
            TagLib.File file = TagLib.File.Create(audioPath, mimeType,TagLib.ReadStyle.Average);
            double minisec = file.Properties.Duration.TotalMilliseconds;
            file.Dispose();
            return minisec;
        }
    }
}