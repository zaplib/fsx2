﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using ZapLib;
using ZapLib.Json;
using ZapLib.Security;
using ZapLib.Utility;

namespace FSX.Lib
{
    public class SQLite
    {
        private string dbPath;
        private string connStr;
        Lazy<MyLog> mylog = new Lazy<MyLog>();

        public string ErrorMessage { get; private set; }

        public string DbVersion { get; private set; }

        public SQLite(string dbPath = null, string dbName = "db.sqlite")
        {
            string SQLiteStorage = dbPath ?? Config.Get("SQLiteStorage");
            this.dbPath = SQLiteStorage == null ? new ServerPath(dbName).FilePath : Path.Combine(SQLiteStorage, dbName);
            connStr = "data source=" + this.dbPath;
            if (!initDB()) throw new Exception("Can not Init DB");
            if (!patch()) throw new Exception("Can not Upgrade DB");
        }

        public bool initDB(string init_script_file = "init.sql")
        {        
            if (File.Exists(dbPath)) return true;
            ServerPath init_script_path = new ServerPath(@"SQLite\"+ init_script_file);
            if(!init_script_path.Exist) return false;
            string intsql = File.ReadAllText(init_script_path.FilePath);
            return quickExec(intsql);
        }

        // patch sql
        public bool patch(string install_config_file = "install.json")
        {
            // db is not exists
            if (!File.Exists(dbPath)) return false;

            // patch config file is not exists
            ServerPath init_install_path = new ServerPath(@"SQLite\"+ install_config_file);
            if (!init_install_path.Exist) return false;

            // patch config data is unknow
            string config_json = File.ReadAllText(init_install_path.FilePath);
            JsonReader jr = new JsonReader();
            ArrayTuple patch_config = (ArrayTuple)jr.Parse(config_json);
            if (patch_config == null) return false;

            // create and get version file content
            ServerPath version_file_path = new ServerPath(@"SQLite\" + "version");
            if(!version_file_path.Exist) File.WriteAllText(version_file_path.FilePath, "Don't edit this file!"+ Environment.NewLine);
           
            foreach (IJsonTuple el in patch_config.Value())
            {
                // get patch string
                string versionstr = File.ReadAllText(version_file_path.FilePath);

                // get patch info
                string version = el["version"].Value<string>();
                string patchfile = el["file"].Value<string>();
                if (string.IsNullOrWhiteSpace(version) || string.IsNullOrWhiteSpace(patchfile)) continue;

                // check version was patched
                Crypto cy = new Crypto();
                string hashversion = cy.Md5(version + patchfile);
                DbVersion = hashversion;
                if (versionstr.Contains(hashversion)) continue;

                // get patch file
                ServerPath patch_file_path = new ServerPath(@"SQLite\" + patchfile);
                if (!patch_file_path.Exist) continue;
                string path_sql = File.ReadAllText(patch_file_path.FilePath);

                // patch sql and log version
                if (quickExec(path_sql)) File.AppendAllText(version_file_path.FilePath, hashversion + Environment.NewLine);
                else return false;
            }
            return true;
        }

        public IEnumerable<dynamic> quickQuery(string sql, object param = null)
        {
            ErrorMessage = "";
            using (var cn = new SQLiteConnection(connStr))
            {
                try
                {
                    return cn.Query(sql, param);
                }
                catch (Exception e)
                {
                    log("SQL: " + sql);
                    log(e.ToString());
                    ErrorMessage = e.ToString();
                    return null;
                }
            }
        }

        public bool quickExec(string sql, object param = null)
        {
            ErrorMessage = "";
            using (var cn = new SQLiteConnection(connStr))
            {
                try
                {
                    cn.Execute(sql, param);
                    return true;
                }
                catch (Exception e)
                {
                    log("SQL: " + sql);
                    log(e.ToString());
                    ErrorMessage = e.ToString();
                    return false;
                }
            }
        }

        private void log(string msg)
        {
            mylog.Value.Write(msg);
        }
    }
}