﻿using System.IO;
using System.Web;

namespace FSX.Lib
{
    public class ServerPath
    {
        public string FilePath { get; private set; }
        public FileAttributes attr { get; private set; }


        public bool Exist { get; private set; }

        public bool IsDirectory
        {
            get => attr.HasFlag(FileAttributes.Directory);
        }

        public ServerPath(string rel_path)
        {
            try
            {
                FilePath = Path.Combine(HttpContext.Current.Server.MapPath("~"), rel_path);
                attr = File.GetAttributes(FilePath);
                Exist = true;
            }
            catch { }         
        }
    }
}