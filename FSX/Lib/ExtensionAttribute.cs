﻿using System;

namespace FSX.Lib
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
    public class ExtensionAttribute : Attribute
    {
        public string TypeName { get; set; }
        public ExtensionAttribute(string type)
        {
            TypeName = type;
        }
    }
}