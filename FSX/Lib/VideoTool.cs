﻿using MediaToolkit;
using MediaToolkit.Model;
using MediaToolkit.Options;
using System;
using System.Diagnostics;
using System.IO;

namespace FSX.Lib
{
    public class VideoTool
    {
        private MediaFile mediaFile;

        public VideoTool(string videoPath)
        {
            mediaFile = new MediaFile { Filename = videoPath };
        }

        public MediaFile GetMetadata()
        {
            using (var engine = new Engine())
            {
                engine.GetMetadata(mediaFile);
            }
            return mediaFile;
        }

        public void createThumbnail(string imgDist)
        {
            var newDist = imgDist + ".jpg";
            using (var engine = new Engine())
            {
                engine.GetMetadata(mediaFile);
                double mediaSec = mediaFile.Metadata.Duration.TotalSeconds;
                Trace.WriteLine("video length (sec): " + mediaSec);
                int helfSec = (int)mediaSec / 2;
                var outputFile = new MediaFile { Filename = newDist };
                var options = new ConversionOptions { Seek = TimeSpan.FromSeconds(helfSec) };
                engine.GetThumbnail(mediaFile, outputFile, options);
                if (File.Exists(newDist)) File.Move(newDist, imgDist);
            }
        }

        public void ScaleToVGA(string outputDist)
        {
            var outputFile = new MediaFile { Filename = outputDist };
            var conversionOptions = new ConversionOptions
            {

                //MaxVideoDuration = TimeSpan.FromSeconds(30),
                VideoAspectRatio = VideoAspectRatio.R4_3,
                VideoSize = VideoSize.Vga,
                AudioSampleRate = AudioSampleRate.Hz44100
            };

            using (var engine = new Engine())
            {
                engine.Convert(mediaFile, outputFile, conversionOptions);
            }
        }


        public bool SmoothScaleToVGA(string outputDist)
        {
            mediaFile = GetMetadata();
            string[] size = mediaFile.Metadata.VideoData.FrameSize.Split('x');
            if (size.Length != 2) return false;
            int w = 0, h = 0;
            if (!int.TryParse(size[0], out w) || !int.TryParse(size[1], out h)) return false;

            double scaleW = 640.0 / w,
                   scaleH = 480.0 / h,
                   scaleS = h * scaleW;

            var conversionOptions = new ConversionOptions
            {
                VideoSize = VideoSize.Custom,
                AudioSampleRate = AudioSampleRate.Hz44100
            };

            if (scaleS <= 480)
            {
                conversionOptions.CustomWidth = 640;
                conversionOptions.CustomHeight = (int)scaleS;
            }
            else
            {
                conversionOptions.CustomWidth = (int)(w * scaleH);
                conversionOptions.CustomHeight = 480;
            }

            var outputFile = new MediaFile { Filename = outputDist };

            using (var engine = new Engine())
            {
                engine.Convert(mediaFile, outputFile, conversionOptions);
            }

            return true;
        }
    }
}