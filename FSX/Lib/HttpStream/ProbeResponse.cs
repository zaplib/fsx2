﻿using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace FSX.Lib.HttpStream
{
    /// <summary>
    /// 影音預先偵測專用回應 HTTP 物件
    /// </summary>
    public class ProbeResponse
    {
        /// <summary>
        /// 主要 HTTP 回應物件
        /// </summary>
        public HttpResponseMessage Response { get; private set; }

        /// <summary>
        /// 主要檔案資訊物件
        /// </summary>
        public FileInfo FileInfo { get; private set; }

        public ProbeResponse(string FilePath)
        {
            Response = new HttpResponseMessage();
            Response.Content = new StreamContent(Stream.Null);

            if (File.Exists(FilePath))
            {
                FileInfo = new FileInfo(FilePath);
                Response.Headers.AcceptRanges.Add("bytes");
                Response.Content.Headers.ContentLength = FileInfo.Length;
                Response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(FileInfo.Name));
            }
            else
            {
                Response.StatusCode = HttpStatusCode.NotFound;
            }
        }
    }

}