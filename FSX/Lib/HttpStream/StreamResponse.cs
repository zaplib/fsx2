﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using ZapLib;
using ZapLib.Utility;

namespace FSX.Lib.HttpStream
{
    /// <summary>
    /// 高階串流 HTTP 回應物件，它實作了"部分內容"與"傳輸限速"的功能，當然也可透過反向注入的方式取代內建實作的方法
    /// </summary>
    public class StreamResponse : IPartialContent, IReateLimit
    {
        /// <summary>
        /// HTTP 回應的物件
        /// </summary>
        public HttpResponseMessage Response { get; set; }

        /// <summary>
        /// 檔案讀取起始位置，預設0
        /// </summary>
        public long StartPoint { get; private set; }

        /// <summary>
        /// 檔案讀取結束位置，預設為最後
        /// </summary>
        public long EndPoint { get; private set; }

        /// <summary>
        /// 實體檔案物件
        /// </summary>
        public FileInfo FileInfo { get; set; }

        /// <summary>
        /// 串流緩衝區大小，預設為 1MB
        /// </summary>
        public long BufferLength { get; set; } = 1048576;

        /// <summary>
        /// 基於程式啟動到開始進行串流工作經過的豪秒數
        /// </summary>
        public long ProcessStartMs { get; private set; }

        /// <summary>
        /// 每秒最大傳輸 byte 大小，預設 0 表示不限制
        /// </summary>
        public long MaximumBytesPerSecond { get; set; }

        /// <summary>
        /// 總傳輸 byte 大小 (用於計算整體傳輸速率)
        /// </summary>
        public long BufferByteCount { get; private set; }

        /// <summary>
        /// Response 內容的大小，部分內容時應該回復部分內容大小
        /// </summary>
        public long ContentLength { get; set; }

        /// <summary>
        /// 是否為部分請求
        /// </summary>
        public bool IsPartial { get; set; }

        /// <summary>
        /// 檔案原始檔名
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 檔案原始 MimeType
        /// </summary>
        public string MimeType { get; set; }

        private MyLog log;
        private MyLog flog;

        /// <summary>
        /// 使用內建功能建構子，初始化 HTTP 回應與檔案訊息物件
        /// </summary>
        /// <param name="FilePath">實體檔案路徑</param>
        /// <param name="StartPoint">讀檔開始位置，預設為 0</param>
        /// <param name="EndPoint">讀檔結束位置，預設為檔案的最後位置</param>
        /// <param name="MaximumBytesPerSecond">每秒最大傳輸 byte 大小(下載限速)，預設為 0 不限速</param>
        public StreamResponse(string FilePath, long StartPoint = 0, long? EndPoint = null, long MaximumBytesPerSecond = 0)
        {
            log = new MyLog();
            log.SilentMode = Config.Get("MediaDebugSilent");

            flog = new MyLog();
            flog.SilentMode = Config.Get("MediaDebugSilent");
            flog.Name = "file";

            Response = new HttpResponseMessage();

            ProcessStartMs = Environment.TickCount;
            this.StartPoint = StartPoint;

            if (MaximumBytesPerSecond == 0)
            {
                string rl = Config.Get("RateLimit");
                if (rl != null) long.TryParse(rl, out MaximumBytesPerSecond);
            }

            this.MaximumBytesPerSecond = MaximumBytesPerSecond;

            if (File.Exists(FilePath))
            {
                FileInfo = new FileInfo(FilePath);
                this.EndPoint = EndPoint == null ? FileInfo.Length - 1 : EndPoint.Value;
            }
            else
            {
                this.EndPoint = EndPoint == null ? 0 : EndPoint.Value;
                Response.StatusCode = HttpStatusCode.NotFound;
            }
        }

        /// <summary>
        /// 取得最終 HTTP 回應物件，該物件已經封裝了指定的限速或部分內容的結果
        /// </summary>
        /// <returns></returns>
        public void StartProcessing()
        {
            if (FileInfo == null) return;
            ProcessStartMs = Environment.TickCount;
            string mime = MimeType ?? MimeMapping.GetMimeMapping(FileName ?? FileInfo.Name);
            MediaTypeHeaderValue mType = new MediaTypeHeaderValue(mime);
            Response.Content = new PushStreamContent(OnStreamAvailable, mType);
            //Response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(FileInfo.Name));
            Response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue(IsInlineContentDisposition(mime) ? "inline": "attachment");
            Response.Content.Headers.ContentDisposition.FileName = FileName ?? FileInfo.Name;
            //Response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
            if (IsPartial)
            {
                Response.StatusCode = HttpStatusCode.PartialContent;
                ContentLength = EndPoint - StartPoint + 1;
                // 是 Partial 請求的時候應該要明確回覆範圍，但是不應該再回復 accept partial 因為已經是
                Response.Content.Headers.ContentRange = new ContentRangeHeaderValue(StartPoint, EndPoint, FileInfo.Length);
            }
            else
            {
                ContentLength = FileInfo.Length;
                // 不是 Partial 請求的時候，要告訴瀏覽器接受 range 請求
                // 只有多媒體才允許範圍請求
                if(MimeType.Contains("video") || MimeType.Contains("audio"))
                    Response.Headers.AcceptRanges.Add("bytes");
                Response.StatusCode = HttpStatusCode.OK;
            }
            Response.Content.Headers.ContentLength = ContentLength;
        }

        /// <summary>
        /// 將內容寫入串流作為輸出，該方法會由 PushStreamContent 承接並呼叫，它會在資料可以開始被串流輸出後呼叫
        /// </summary>
        /// <param name="outputStream">輸出的串流物件，需實作將資料寫入這個串流</param>
        /// <param name="content">代表 HTTP 實體內容和內容標頭的基底類別</param>
        /// <param name="context">提供有關基礎傳輸層的其他內容</param>
        private async void OnStreamAvailable(Stream outputStream, HttpContent content, TransportContext context)
        {
            using (FileStream fs = FileInfo.OpenRead())
            {
                foreach ((byte[] buffer, int bytesRead) in GetPartialContent(fs))
                {
                    // 累積目前總處理 byte 量
                    BufferByteCount += bytesRead;

                    // 限速機制
                    int DelayMs = GetDalayMs(bytesRead);
                    try
                    {
                        //await outputStream.WriteAsync(buffer, 0, bytesRead);
                        // 延遲後再寫出
                        if (DelayMs > 0) await Task.Delay(DelayMs);
                        await outputStream.WriteAsync(buffer, 0, bytesRead)                
                        .ContinueWith(e => log.Write($"{DateTime.Now} [>] Write Data: {bytesRead} byte to Streaming, Count: {BufferByteCount} ({(int)((100 * BufferByteCount) / ContentLength)} %) Delay:{DelayMs} RateLimit {MaximumBytesPerSecond}"))
                        .ContinueWith(e => flog.Write(BitConverter.ToString(buffer)));            
                    }
                    catch
                    {
                        // 用戶端關閉連結，停止再讀資料
                        log.Write($"{DateTime.Now}--------------------- 用戶端關閉連結 ouput stream be closed, stop read file ---------------------");
                        break;
                    }
                    finally
                    {
                        outputStream.Close();
                        log.Write($"{DateTime.Now}------------------------------------100%------------------------------------------");
                    }
                }
            }
        }

        /// <summary>
        /// 迭代器：使用檔案讀取串流將檔案讀進緩衝區回傳，直到整份檔案被讀完才會結束迭代
        /// </summary>
        /// <param name="fs">檔案讀取串流</param>
        /// <param name="currentPoint">從哪一個位置開始讀</param>
        /// <returns>會傳 Tuple 一整個緩衝區的資料與讀取長度</returns>
        public IEnumerable<(byte[] buffer, int bytesRead)> GetPartialContent(Stream fs)
        {
            long currentPoint = StartPoint;

            if (currentPoint > EndPoint) yield break;
            // 尚未讀取的長度
            long bytesLeft = EndPoint - currentPoint + 1;
            // 位移讀取指針
            fs.Seek(currentPoint, SeekOrigin.Begin);

            byte[] buffer = new byte[BufferLength];

            // 迭代讀取檔案，直到指定的結束位置
            while (currentPoint <= EndPoint)
            {
                // 只能讀取最多 buffer 的長度 (剩餘長度不能超過 buffer 長度)
                int readLength = (int)Math.Min(bytesLeft, buffer.Length);
                int bytesRead = fs.Read(buffer, 0, readLength);
                log.Write($"{DateTime.Now} [<] Read Data:  From: {currentPoint} To {currentPoint + bytesRead}, EndPoint: {EndPoint} ({(int)((100 * (currentPoint + bytesRead)) / EndPoint)} %)");
                // 讀取成功，把指針更新 (寫出如果失敗不會再進來)
                currentPoint += bytesRead;
                // 剩餘長度更新
                bytesLeft -= bytesRead;
                yield return (buffer, bytesRead);
            }
        }

        /// <summary>
        /// 計算需要延遲多少毫秒，才能讀下一輪的 buffer (Throttle)
        /// </summary>
        /// <param name="BufferByteCount">這一輪輸出的 buffer 大小</param>
        /// <returns>需要等待幾秒，毫秒部分無條件捨去</returns>
        public int GetDalayMs(int CurrentBufferByte)
        {
            // 不限速或緩衝為 0 無需等待
            if (MaximumBytesPerSecond <= 0 || CurrentBufferByte <= 0) return 0;

            // 程式開始到現在經過多少毫秒
            long elapsedMs = Environment.TickCount - ProcessStartMs;

            // 確保程式已經開始，否則無法計算傳輸速度
            if (elapsedMs <= 0) return 0;

            // 目前傳輸速率 (bps = byte per second)
            long bps = BufferByteCount * 1000L / elapsedMs;

            // 速率在限制的門檻下，無須等待
            if (bps <= MaximumBytesPerSecond) return 0;

            // 由目前傳輸總量與限制速率，估算預期應該花費多少毫秒
            long expectElapsedMs = BufferByteCount * 1000L / MaximumBytesPerSecond;

            // 等待時間 = 預期花費時間 - 實際執行時間
            return (int)(expectElapsedMs - elapsedMs);
        }

        private bool IsInlineContentDisposition(string mimne)
        {
            if (mimne.Contains("video")) return true;
            else if(mimne.Contains("audio")) return true;
            else if (mimne.Contains("image")) return true;
            else if (mimne.Contains("pdf")) return true;
            else if (mimne.Contains("text")) return true;
            return false;
        }
    }

}