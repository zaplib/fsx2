﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;

namespace FSX.Lib
{
    public class ImageTool
    {
        private string imgPath;
        public ImageTool(string imgPath)
        {
            this.imgPath = imgPath;
        }

        public Tuple<int, int> getSize()
        {
            int width = 0, height = 0;
            using (var srcImage = Image.FromFile(imgPath))
            {
                width = srcImage.Width;
                height = srcImage.Height;
            }
            return Tuple.Create(width, height);
        }

        public void scale(int maxSize)
        {
            if (!File.Exists(imgPath)) return;
            var srcImage = Image.FromFile(imgPath);
            if ((srcImage.Width <= maxSize) && (srcImage.Height <= maxSize)) return;
            double scaleFactor = (double)maxSize / (double)Math.Max(srcImage.Width, srcImage.Height);
            //Trace.WriteLine("scaleFactor: " + scaleFactor);
            int newWidth = (int)(srcImage.Width * scaleFactor);
            int newHeight = (int)(srcImage.Height * scaleFactor);
            //Trace.WriteLine("scale width: " + newWidth);
            //Trace.WriteLine("scale height: " + newHeight);
            var newImage = new Bitmap(newWidth, newHeight);
            var graphics = Graphics.FromImage(newImage);
            graphics.SmoothingMode = SmoothingMode.AntiAlias;
            graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
            graphics.DrawImage(srcImage, new Rectangle(0, 0, newWidth, newHeight));
            srcImage.Dispose();  
            newImage.Save(imgPath);
            graphics.Dispose();
            newImage.Dispose();      
        }
    }
}