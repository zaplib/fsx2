﻿using System;
using System.Reflection;
using ZapLib.Utility;

namespace FSX.Lib
{
    public class ClassMirror
    {
        public Type type { get; private set; }
        public object Instance { get; private set; }
        public ClassMirror(Type type, params object[] args)
        {
            this.type = type;
            try
            {
                Instance = Activator.CreateInstance(this.type, args);
            }
            catch { }
        }

        public MethodInfo this[string MethodName] => GetMethod(MethodName);


        private MethodInfo GetMethod(string MethodName)
        {           
            try
            {
                if (Instance == null) return null;
                return  type.GetMethod(MethodName);

            }
            catch{}
            return null;
        }

    }
}