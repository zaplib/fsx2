﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ZapLib.Utility;

namespace FSX.Lib
{
    public static class Mirror2
    {
        public static IEnumerable<Type> GetClasses<T>(bool include_self = false)
        {
            Assembly asm = Assembly.GetAssembly(typeof(T));
            return asm.GetTypes().Where(t => {
                //Trace.WriteLine("Assembly.GetTypes: " + t.Name);
                return include_self ?
                ( typeof(T).Equals(t) || typeof(T).IsAssignableFrom(t)):
                (!typeof(T).Equals(t) && typeof(T).IsAssignableFrom(t));
            });
        }

        public static IEnumerable<T> GetCustomAttributes<T>(Type prop) 
        {
            foreach (var atr in prop.GetCustomAttributes(true))
            {
                if (Cast.IsType<T>(atr))
                {
                    yield return (T)atr;
                }
            }
        }

    }
}