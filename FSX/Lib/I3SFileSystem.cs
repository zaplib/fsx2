﻿using FSX.Models;
using System;
using System.IO;
using System.Linq;
using System.Web;
using ZapLib.Utility;

namespace FSX.Lib
{
    public class I3SFileSystem
    {
        private string rootPath;

        public I3SFileSystem(string rootPath = null)
        {
            this.rootPath = string.IsNullOrWhiteSpace(rootPath) ? Config.Get("Storage") : rootPath;
            if (!Directory.Exists(this.rootPath)) throw new Exception("Can not get File System Root Directory");
        }

        public string convertIdToPath(long oid)
        {
            string HexPath = Convert.ToString(oid, 16);
            while (HexPath.Length < 8) HexPath = "0" + HexPath;
            HexPath = HexPath.Insert(2, "\\");
            HexPath = HexPath.Insert(5, "\\");
            HexPath = HexPath.Insert(8, "\\");
            checkDirExist(HexPath);
            return rootPath + "\\" + HexPath;
        }

        private void checkDirExist(string path)
        {
            string[] sp = path.Split('\\');
            string treePath = rootPath;
            if (!Directory.Exists(treePath)) Directory.CreateDirectory(treePath);
            for (int i = 0; i < sp.Length; i++)
            {
                if (!Directory.Exists(treePath)) Directory.CreateDirectory(treePath);
                treePath += "\\" + sp[i];
            }
        }


        public ModelRegisterFile register(string filename = null)
        {
            SQLite db = new SQLite();
            string uuid = Guid.NewGuid().ToString();
            if (string.IsNullOrWhiteSpace(filename)) filename = uuid;
            string mimeType = MimeMapping.GetMimeMapping(filename);
            var res = db.quickQuery("insert into Object(uuid,cname,ename)values(@uuid,@mimeType,@ename); SELECT last_insert_rowid() as oid", new { uuid, mimeType, ename = filename });
            if (res == null) return null;
            long oid = res.Single().oid;
            string dist = convertIdToPath(oid);
            return new ModelRegisterFile()
            {
                oid = oid,
                dist = dist,
                uuid = uuid,
                mimeType = mimeType,
                filename = filename
            };
        }

        public ModelRegisterFile getRegisterFile(string uuid)
        {
            SQLite db = new SQLite();
            var res = db.quickQuery("select * from Object where uuid=@uuid", new { uuid });
            if (res == null) return null;
            try
            {
                dynamic row = res.Single();
                string filename = row.ename;
                if (string.IsNullOrWhiteSpace(filename)) filename = uuid;
                return new ModelRegisterFile()
                {
                    oid = row.oid,
                    dist = convertIdToPath(row.oid),
                    uuid = row.uuid,
                    mimeType = row.cname,
                    filename = filename
                };
            }
            catch
            {
                return null;
            }

        }


    }
}