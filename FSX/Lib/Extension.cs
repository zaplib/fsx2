﻿using System.Collections.Generic;
using System.Net.Http;
using System.Security.Principal;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.ModelBinding;
using System.Web.Http.Routing;

namespace FSX.Lib
{
    public abstract class Extension
    {

        public ApiController Controller;

        public Dictionary<string, string> Query { get; private set; }

        public void SetController(ApiController Controller)
        { 
            this.Controller = Controller;
            Query = new Dictionary<string, string>();
            foreach (var kv in Controller.Request.GetQueryNameValuePairs())
            {
                Query.Add(kv.Key, kv.Value);
            }
        }

        public abstract HttpResponseMessage GetResponse();
    }
}
