﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FSX.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FSX.Tests.TestClasses;
using System.Reflection;
using ZapLib.Utility;

namespace FSX.Lib.Tests
{
    [TestClass()]
    public class ClassMirrorTests
    {
        [TestMethod()]
        public void ClassMirrorTest()
        {
            foreach(Type type in Mirror2.GetClasses<ITest>())
            {
                ClassMirror cls = new ClassMirror(type);
                var EchoFunc = cls["Echo"];
                var PrintFunc = cls["Print"];
                Assert.IsNotNull(EchoFunc);
                Assert.IsNotNull(PrintFunc);
            }
        }

        [TestMethod()]
        public void CallTest()
        {
            foreach (Type type in Mirror2.GetClasses<ITest>())
            {
                ClassMirror cls = new ClassMirror(type);
                MethodInfo EchoFunc = cls["Echo"];
                Assert.IsNotNull(EchoFunc);
                object Echo_res = EchoFunc.Invoke(cls.Instance, new object[] { "this is a test" });
                StringAssert.Contains(Cast.To<string>(Echo_res), "this is a test");

                MethodInfo PrintFunc = cls["Print"];
                Assert.IsNotNull(PrintFunc);
                object Print_res = PrintFunc.Invoke(cls.Instance, new object[] { "this is a test" });
                Assert.IsNull(Print_res);
            }
        }
    }
}