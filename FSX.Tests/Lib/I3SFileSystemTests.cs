﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FSX.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace FSX.Lib.Tests
{
    [TestClass()]
    public class I3SFileSystemTests
    {
        [TestMethod()]
        public void register()
        {
            I3SFileSystem fs = new I3SFileSystem();
            var res = fs.register();
            Trace.WriteLine(res.oid);
            Trace.WriteLine(res.uuid);
            Trace.WriteLine(res.dist);
            Trace.WriteLine(res.mimeType);
        }

        [TestMethod()]
        public void getRegisterFile()
        {
            I3SFileSystem fs = new I3SFileSystem();
            var res = fs.getRegisterFile("0c5ec2fb-ff8e-4b52-a653-e645e781c0e9");
            Trace.WriteLine(res.oid);
            Trace.WriteLine(res.uuid);
            Trace.WriteLine(res.dist);
            Trace.WriteLine(res.mimeType);

        }
    }
}