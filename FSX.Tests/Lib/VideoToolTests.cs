﻿using FSX.Lib;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System.Diagnostics;

namespace FSX.Lib.Tests
{
    [TestClass()]
    public class VideoToolTests
    {
        [TestMethod()]
        public void createThumbnail()
        {
            VideoTool vtool = new VideoTool(@"D:\FSX_Storage\00\00\00\0a");
            I3SFileSystem fs = new I3SFileSystem();
            var outputImg = fs.register("thumb.jpg");
            vtool.createThumbnail(outputImg.dist);
            Trace.WriteLine("thumb dist: " + outputImg.dist);
            Trace.WriteLine("thumb uuid: " + outputImg.uuid);
        }

        [TestMethod()]
        public void ScaleToVGA()
        {
            VideoTool vtool = new VideoTool(@"D:\FSX_Storage\sample2.mp4");
            vtool.ScaleToVGA(@"D:\FSX_Storage\output\sample2.mp4");
        }


        [TestMethod()]
        public void GetMetadata()
        {
            VideoTool vtool1 = new VideoTool(@"D:\FSX_Storage\sample1.mp4");
            VideoTool vtool2 = new VideoTool(@"D:\FSX_Storage\sample2.mp4");

            VideoTool vtool3 = new VideoTool(@"D:\FSX_Storage\output\(20sec)sample1-1.mp4");
            VideoTool vtool4 = new VideoTool(@"D:\FSX_Storage\output\(1800sec)sample2-1.mp4");

            Trace.WriteLine(JsonConvert.SerializeObject(vtool1.GetMetadata()));
            Trace.WriteLine(JsonConvert.SerializeObject(vtool3.GetMetadata()));

            Trace.WriteLine(JsonConvert.SerializeObject(vtool2.GetMetadata()));
            Trace.WriteLine(JsonConvert.SerializeObject(vtool4.GetMetadata()));
        }



        [TestMethod()]
        public void SmoothScaleToVGA()
        {
            VideoTool vtool = new VideoTool(@"D:\FSX_Storage\sample2.mp4");
            var res = vtool.SmoothScaleToVGA(@"D:\FSX_Storage\output\sample2-1.mp4");
            Assert.IsTrue(res);
        }
    }
}