﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FSX.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace FSX.Lib.Tests
{
    [TestClass()]
    public class SQLiteTests
    {
        [TestMethod()]
        public void quickQuery()
        {
            SQLite db = new SQLite();
            string uuid = Guid.NewGuid().ToString();
            var res = db.quickQuery("insert into Object(uuid)values(@uuid); SELECT last_insert_rowid() as oid", new { uuid });
            if (res == null) Trace.WriteLine("Fail");         
            else
            {
                long oid = res.Single().oid;
                Trace.WriteLine(oid);
            }
        }
    }
}