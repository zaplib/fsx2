﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FSX.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FSX.Tests.TestClasses;
using System.Diagnostics;

namespace FSX.Lib.Tests
{
    [TestClass()]
    public class Mirror2Tests
    {
        [TestMethod()]
        public void GetClassesTest()
        {
            int i = 0;
            foreach(Type t in Mirror2.GetClasses<ITest>())
            {
                Trace.WriteLine(t.Name);
                i++;
            }
            Assert.AreEqual(2, i);
        }

        [TestMethod()]
        public void GetClassesTest2()
        {
            int i = 0;
            foreach (Type t in Mirror2.GetClasses<ITest>(true))
            {
                Trace.WriteLine(t.Name);
                i++;
            }
            Assert.AreEqual(3, i);
        }



    }


}