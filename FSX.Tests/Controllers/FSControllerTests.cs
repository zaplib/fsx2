﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;
using System.Text;
using ZapLib;

namespace FSX.Controllers.Tests
{
    [TestClass()]
    public class FSControllerTests
    {
        [TestMethod()]
        public void upload()
        {
            Fetch f = new Fetch("http://localhost:64516/api/upload");
            byte[] byteArray = Encoding.ASCII.GetBytes("123456");
            string res = f.Post(new
            {
                name = "my.txt",
                file = byteArray
            });
            Trace.WriteLine(res);
        }
    }
}