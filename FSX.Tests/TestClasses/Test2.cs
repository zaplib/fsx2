﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSX.Tests.TestClasses
{
    public class Test2 : ITest
    {
        public string Echo(string str)
        {
            return GetType().Name + " : " + str;
        }

        public void Print(string str)
        {
            Trace.WriteLine(GetType().Name + " : " + str);
        }
    }
}
