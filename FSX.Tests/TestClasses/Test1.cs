﻿using System.Diagnostics;

namespace FSX.Tests.TestClasses
{
    public class Test1 : ITest
    {
        public string Echo(string str)
        {
            return GetType().Name + " : " + str;
        }

        public void Print(string str)
        {
            Trace.WriteLine(GetType().Name + " : " + str);
        }
    }
}
