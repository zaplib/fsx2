﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSX.Tests.TestClasses
{
    public interface ITest
    {
        string Echo(string str);
        void Print(string str);
    }
}
