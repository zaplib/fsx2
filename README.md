# FSX 2 - Streaming

File Storage and Streaming Extension Microservice 

## Demo

#### [FSX 2 - Demo](http://192.168.1.102:8888/)

## Launch

若您要自行架設請依照以下說明進行安裝：  

下載本專案，重建並編譯後，將結果佈署至 IIS 站台
```
git clone http://192.168.1.136/SideProject/FSX.git
```
  
佈署後，請開啟 `Web.config` 設定 `<appSettings>` 區域的以下欄位：

* `Storage` : 上傳檔案與 Log 的儲存的根目錄

承上，以下項目均有預設值，可視需求設定：

* `RateLimit` : 下載限速，單位為 `byte`，如果沒有設定或設定為 `0` 則表示不限速
* `BufferLength` : 下載緩衝大小，單位為 `byte`，預設為 `245760`
* `ImageCompression` : 上傳圖片最大寬高，超過會自動等比縮小，單位為 `px`，如果沒有設定預設為 `1024`
* `MediaDebugSilent` : 關閉開發人員測試模式，預設為 `true` 關閉
* `Host` : 強制設定檔案下載的 url 的 host 位置，沒有設定預設為目前主機的伺服器位置
* `SQLiteStorage` : SQLite 資料庫存放位置，沒有設定預設放在專案根目錄下
* `SilentMode` : 關閉所有內建的 Log 輸出，如果沒有設定則預設為 `false`，表示不關閉


## Package

本專案依賴以下第三方套件

* [MediaToolkit 1.1.0.1](https://www.nuget.org/packages/MediaToolkit/)
* [AspNet.Cors 5.2.7](https://www.nuget.org/packages/Microsoft.AspNet.Cors/)
* [Dapper 1.42.0](https://www.nuget.org/packages/Dapper/)
* [ZapLib 2.0.7](https://www.nuget.org/packages/ZapLib/)
* [SQLite.Core 1.0.110](https://www.nuget.org/packages/System.Data.SQLite.Core/)

> 套件依賴的套件不逐一詳列

## Basic API

以下為 FSX 2 的統一版 API

### Upload File

上傳檔案，支援兩種輸入格式 (content-type)，如下說明：
  
```
POST /api/upload
```
  
**body**

第一種格式 `multipart/form-data`

| field | type | description |
| ----- | ----------- | ----------- |
| file  | Binary[]    | 檔案        |


第二種格式 `application/json`

| field | type | description |
| ----- | ----------- | ----------- |
| name  | string      | 檔案名稱       |
| file  | string    | 原始檔案以 Base64 編碼 |

**output**

Content-Type 一律為 `application/json`

| field | type | description |
| ----- | ----------- | ----------- |
| filename | string    | 檔案原始名稱(含附檔名)  |
| uuid     | string    | 檔案唯一 ID  |
| mimeType | string    | 檔案的 mimeType  |
| url | string    | 檔案的下載 URL  |
| length | long    | 檔案大小 (byte)  |
| width | int    | 圖片寬度 (`mimeType`必須要為`image/*`才會有)  |
| height | int    | 圖片長度 (`mimeType`必須要為`image/*`才會有)  |
| thumbnail | string    | 影片縮圖 URL (`mimeType`必須要為`video/*`才會有)  |
| duration | double    | 音訊的長度 (`mimeType`必須要為`audio/*`才會有)  |

以下為上傳 `123.jpg` 的輸出的實例：

```json
{
    "width": 1024,
    "height": 683,
    "mimeType": "image/jpeg",
    "uuid": "5a3c8fa1-a5a1-46dd-877d-5b89632bafee",
    "filename": "123.jpg",
    "length": 918399,
    "url": "http://192.168.1.102:8888/api/download?uuid=5a3c8fa1-a5a1-46dd-877d-5b89632bafee"
}
```

**note**

* 如果上傳的檔案 MIME-Type 為 `video/*` 會自動取得影片中間位置的縮圖，縮圖大小為 240 等比縮放
* 如果上傳的檔案 MIME-Type 為 `audio/*` 會自動取得音訊的長度，以 milliseconds 表示
 
### Download File

下載檔案，請使用上傳檔案所回傳的 `uuid` 進行下載

```
GET /api/download
```

**query param**

| field | type  | required | description |
| ----- | ----  | ----- | ----------- |
| uuid | string | N | 檔案 ID  |
| info | bool   | N | 是否取得檔案資訊，預設為 `false`  |
| type | string | N | 執行客製化下載功能  |

**request header**

| field | required | description |
| ----- | -------- | --------- |
| Range | N | 在請求 `Content-Type` 為 `audio/*` 或 `video/*` 時，可以取得部分資料 |


#### 一般下載 output 規格

在以下條件成立時，才會依照這個規格，否則請參照客製化功能定義的輸出規格

* `uuid` 有設置且合法
* `info` 為 `false`  
* `type` 沒有設置

**output header**

| field  | description |
| ----- | ----------- |
| Content-Length | 數值為檔案大小 (byte) |
|Content-Range| 該回應的檔案資料範圍，只有在 Request Header 中有設置 `Range` 時才會回應 |
| Content-Type | 數值為檔案的 mime type | 
| Accept-Ranges | 數值為 `byte`，只有在 `Content-Type` 為 `audio/*` 或 `video/*` 時才會設置 |
| Content-Disposition | 只有在 `Content-Type` 為 `image/*`, `audio/*` 或 `video/*` 時為 `inline` 其餘為 `attachment`，並且會帶檔案原始名稱 `; filename="原始檔名"`  |
| Cache-Control | 數值為 `no-cache` |


**output body**

二進位檔案流   


####  檔案資訊 output 規格

在以下條件成立時，才會依照這個規格，否則請參照客製化功能定義的輸出規格

* `uuid` 有設置且合法
* `info` 為 `true`  
* `type` 沒有設置



**output header**
| field  | description |
| ----- | ----------- |
| Content-Type | 數值為 `application/json; charset=utf-8` | 

**output body**

| field | type | description |
| ----- | ----------- | ----------- |
| filename | string    | 檔案原始名稱(含附檔名)  |
| uuid | string    | 檔案 ID  |
| mimeType | string    | 檔案的 mimeType  |
| url | string    | 檔案的下載 URL  |
| length | long    | 檔案大小 (byte)  |
| width | int    | 圖片寬度 (`mimeType`必須要為`image/*`才會有)  |
| height | int    | 圖片長度 (`mimeType`必須要為`image/*`才會有)  |
| thumbnail | string    | 影片縮圖 URL (`mimeType`必須要為`video/*`才會有)  |
| duration  | double    | 音訊的長度 (`mimeType`必須要為`audio/*`才會有)  |

**note**

* 如果輸入非法的 `uuid` 將回傳 403 forbidden
* 如果該 `uuid` 對應不到任何實體檔案 (可能檔案已經在 HDD 被刪除) 將回傳 404 not found


### Video thumbnail

如果上傳了 mimeType 為 `video/*` 類型的檔案，FSX 會自動取得影片的 thumbnail，使用檔案的 `uuid` 來取得

```
GET /api/thumbnail
```

**query param**

| field | type | description |
| ----- | ----------- | ----------- |
| uuid | string    | 檔案 ID  |

**output**

二進位檔案流    

> 注意，這邊的檔案 MIME-Type 一率為 `image/jpg`，且圖片大小會在 240 X 240 以下 (等比例縮放)



## Extension API

從 FSX 2 開始，允許擴充 `api/download` API，其方式為定義自訂的 query params `type`，擴充方式如下：

1. 首先在 FSX2 專案下的 `Controllers/Extensions` 目錄下新增 `{Name}Extension.cs` 檔案，內容可以參考以下設定

`TestExtension.cs`

```csharp
using FSX.Lib;
using System.Net.Http;
using ZapLib;

namespace FSX.Controllers.Extensions
{
    [Extension("test")]
    public class TestExension : Extension
    {
        public override HttpResponseMessage GetResponse()
        {
            ExtApiHelper api = new ExtApiHelper(Controller);

            return api.GetResponse(new {
                test = true
            });
        }
    }
}
```

2. 使用 `api/download` API ，可以在 query params 中帶 `type` 參數， FSX2 會搜尋具備 `[Extension("type 名稱")]` 標籤且同名的類別執行自訂 API，例如上面這個例子：

**request**
```
GET /api/downalod?type=test
```

**response**

```json
{
    "test": true
}
```

#### 結論

你可以使用 Extesnion 擴充機制複寫 FSX2 的下載功能，如果你有特殊需要整合下載的需求，它將讓你更具備彈性的擴充


## SQLite 擴充

FSX2 開始允許擴充自定義的 SQLite 表格等，你可以在 FSX2 既有的 SQLite 機制下進行擴充

1. 在專案下的 `SQLite` 目錄下新增要附加的 SQL 語法，並儲存成 `{fileName}.sql` 檔案

`test.sql`

```sql
CREATE TABLE MyData(
   id INTEGER PRIMARY KEY AUTOINCREMENT,
   str CHAR(255) not null
);
```

2. 在目錄下的 `install.json` 註冊該筆附加檔案

`SQLite/install.json`

```json
[
  {
    "version": "v1.1",
    "file": "20181115.sql"
  },
  {
    "version": "1.0",
    "file": "test.sql"
  }
]
```

3. 其中註冊資訊的用途如下說明：

* `version`: 如果 `file` 檔案有改變，可以修改 `version`，將會重新執行該 patch
* `file` : 附加的 SQL 指令檔名

> **注意**：執行完附加的 SQL 之後，會在本目錄下建立一個 `version` 的簽名檔，該簽名檔中會有執行附加 SQL 的版本紀錄，請勿修改或刪除。

## License

  FSX 2 Copyright 2019 Zap

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.   
   